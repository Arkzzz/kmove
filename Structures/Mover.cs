﻿using Lib_K_Relay.Networking.Packets.DataObjects;
using System;

namespace KMove.Structures {
    class Mover {
        public int GetLastSendDiff() {
            int num = Environment.TickCount - LastSent;
            LastSent = Environment.TickCount;

            int result;

            if (num > 200) result = 200;
            else result = num;
            return result;
        }

        public int LastSent = 0;

        public bool UseSafe = false; // Prevents user from moving more than 1 tile/newtick

        public Location Target;

        public bool StopGotoAck = false;

        public TileMap Tiles;
    }
}
