﻿using Lib_K_Relay.Networking.Packets.DataObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KMove.Structures {
    public static class ArrayExtensions {
        public static void Fill<T>(this T[] originalArray, T with) {
            for (int i = 0; i < originalArray.Length; i++) {
                originalArray[i] = with;
            }
        }
    }
    class TileMap {

        private bool[] NoWalk;

        private int Width;

        public TileMap(int width, int height) {
            Width = width;
            NoWalk = new bool[width * height];
            NoWalk.Fill(false);
        }

        public bool Get(double x, double y) {
            return NoWalk[(int)(Math.Floor(y) * Width + Math.Floor(x))];
        }

        public void Set(double x, double y, bool value) {
            if (Get(x, y) == true) return;
            NoWalk[(int)(Math.Floor(y) * Width + Math.Floor(x))] = value;
        }

        public void Set(Location loc, bool value) {
            if (Get(loc) == false) return;
            NoWalk[(int)(Math.Floor(loc.Y) * Width + Math.Floor(loc.X))] = value;
        }

        public bool Get(Location loc) {
            return Get(loc.X, loc.Y);
        }
    }
}
