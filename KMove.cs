﻿using System;
using System.Collections.Generic;
using System.Linq;
using KMove.Structures;
using Lib_K_Relay;
using Lib_K_Relay.GameData;
using Lib_K_Relay.GameData.DataStructures;
using Lib_K_Relay.Interface;
using Lib_K_Relay.Networking;
using Lib_K_Relay.Networking.Packets;
using Lib_K_Relay.Networking.Packets.Client;
using Lib_K_Relay.Networking.Packets.DataObjects;
using Lib_K_Relay.Networking.Packets.Server;
using Lib_K_Relay.Utilities;

namespace KMove {
    internal static class Extender {
        public static void Move(this Client client, Location pos) {
            KMove.Move(client, pos);
        }
        public static void Log(this Client client, string fmt, params object[] args) {
            client.SendToClient(PluginUtils.CreateOryxNotification("KMove", string.Format(fmt, args)));
            PluginUtils.Log("KMove", string.Format(fmt, args));
        }
    }
    public class KMove : IPlugin {
        #region Plugin Stuff
        public string GetAuthor() => "Ark";

        public string[] GetCommands() => new string[] { "/clearmovement - Clears movement target", "/move <x> <y> - Hmmn" };

        public string GetDescription() => "A utility for moving clients with krelay! Credits to PZerg for the original KRelayMove";

        public string GetName() => "KMove";
        
        #endregion Plugin Stuff

        private static Dictionary<Client, Mover> ClientMap = new Dictionary<Client, Mover>();

        public void Initialize(Proxy proxy) {
            #region Proxy Hooks
            proxy.HookPacket(PacketType.NEWTICK, OnNewTick);
            proxy.HookPacket(PacketType.GOTOACK, OnGotoAck);

            proxy.HookCommand("move", OnMoverTest);
            proxy.HookCommand("clearmovement", OnClear);
            proxy.HookPacket(PacketType.UPDATE, OnUpdate);
            proxy.HookPacket(PacketType.MAPINFO, OnMapInfo);
            proxy.ClientConnected += OnConnection;
            #endregion Proxy Hooks
        }

        private void OnClear(Client client, string command, string[] args) {
            if(ClientMap.Keys.Contains(client)) ClientMap[client].Target = null;
        }

        private void OnMoverTest(Client client, string command, string[] args) {
            try {
                Move(client, new Location(client.PlayerData.Pos.X + float.Parse(args[0]), client.PlayerData.Pos.Y + float.Parse(args[1])));
            } catch { }
        }

        private void OnMapInfo(Client client, Packet packet) {
            MapInfoPacket mapInfo = (MapInfoPacket)packet;
            if (ClientMap.Keys.Contains(client)) ClientMap[client].Tiles = new TileMap(mapInfo.Height, mapInfo.Width);
        }

        private void OnUpdate(Client client, Packet packet) {
            UpdatePacket update = (UpdatePacket)packet;
            Mover mover = ClientMap[client];
            foreach (Entity o in update.NewObjs) {
                ObjectStructure obj = GameData.Objects.ByID(o.ObjectType);
                if (obj.OccupySquare || obj.FullOccupy || obj.EnemyOccupySquare || obj.Static || obj.DrawOnGround || obj.BlocksSight) mover.Tiles.Set(o.Status.Position, true);
            }

            foreach (Tile t in update.Tiles) {
                TileStructure tile = GameData.Tiles.ByID(t.Type);
                if (tile.NoWalk) mover.Tiles.Set(t.X, t.Y, true);
            }
        }

        private void OnGotoAck(Client client, Packet packet) {
            Mover mover = ClientMap[client];
            if (mover.StopGotoAck) {
                packet.Send = false;
                mover.StopGotoAck = false;
            }
        }

        public static void Move(Client client, Location pos) {
            Mover mover = ClientMap[client];
            //client.Log("Moving to x: " + pos.X + " y: " + pos.Y);
            if (ClientMap.Keys.Contains(client)) ClientMap[client].Target = pos;
        }

        private void OnNewTick(Client client, Packet packet) {
            NewTickPacket newTick = (NewTickPacket)packet;
            Mover mover = ClientMap[client];
            if (mover.Target == null) return;
            if (mover.Target.X == client.PlayerData.Pos.X && mover.Target.Y == client.PlayerData.Pos.Y) {
                mover.Target = null;
                return;
            };
            double angle = Math.Atan2(mover.Target.X - client.PlayerData.Pos.X, mover.Target.Y - client.PlayerData.Pos.Y);
            float distanceToTarget = client.PlayerData.Pos.DistanceTo(mover.Target);
            float stepDistance = (3f + 5.6f * (client.PlayerData.Speed / 75f)) * (mover.GetLastSendDiff() / 1000f);
            if (mover.UseSafe && stepDistance > 1) stepDistance = .95f;
            //client.Log("Max step distance: " + stepDistance + " Angle: " + angle + " Target Distance:" + distanceToTarget);
            if (distanceToTarget < stepDistance) stepDistance = distanceToTarget;

            GotoPacket step = (GotoPacket)Packet.Create(PacketType.GOTO);
            step.ObjectId = client.ObjectId;

            step.Location = (Location)client.PlayerData.Pos.Clone();

            float x = step.Location.X + (float)Math.Sin(angle) * stepDistance;
            float y = step.Location.Y + (float)Math.Cos(angle) * stepDistance;

            if (!mover.Tiles.Get(x, y)) step.Location = new Location(x, y);

            mover.StopGotoAck = true;
            client.SendToClient(step);
            foreach (Status status in newTick.Statuses) {
                if (status.ObjectId == client.ObjectId) {
                    status.Position = step.Location;
                }
            }
        }

        private void OnConnection(Client client) {
            if (!ClientMap.Keys.Contains(client)) ClientMap.Add(client, new Mover());
        }
    }
}
